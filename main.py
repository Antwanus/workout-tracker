from pprint import pprint
from datetime import datetime
import requests as requests

NUTRITIONIX_APP_ID = '3aeb52d0'
NUTRITIONIX_API_KEY = '9011315391944b5b5acc114f30b47e0d'
NUTRITIONIX_API_URL = 'https://trackapi.nutritionix.com/v2'
NUTRITIONIX_EXERCISE_PATH = '/natural/exercise'
SHEETY_API_URL = 'https://api.sheety.co/47f9c4ec8268d433ed39542adf98442e/twan/workouts'

nutri_headers = {
    'x-app-id': NUTRITIONIX_APP_ID,
    'x-app-key': NUTRITIONIX_API_KEY,
    'Content-Type': 'application/json'
}
nutri_body = {
    "query": input("Provide your workout: "),
    "gender": "female",
    "weight_kg": 72.5,
    "height_cm": 167.64,
    "age": 30
}

response = requests.post(url=NUTRITIONIX_API_URL + NUTRITIONIX_EXERCISE_PATH, headers=nutri_headers, json=nutri_body)
response.raise_for_status()
# pprint(response.json())
# {'exercises': [{'benefits': None,
#                 'compendium_code': 12050,
#                 'description': None,
#                 'duration_min': 100,
#                 'met': 9.8,
#                 'name': 'running',
#                 'nf_calories': 1184.17,
#                 'photo': {'highres': 'https://d2xdmhkmkbyw75.cloudfront.net/exercise/317_highres.jpg',
#                           'is_user_uploaded': False,
#                           'thumb': 'https://d2xdmhkmkbyw75.cloudfront.net/exercise/317_thumb.jpg'},
#                 'tag_id': 317,
#                 'user_input': 'ran'}]}

data_list = []
nutri_response = response.json()['exercises']
sheety_headers = {
    'Authorization': 'Bearer lkmsdfhjmiofhqsm'
}
for record in nutri_response:
    tmp_dict = {'workout': {
        'date': datetime.now().strftime('%d/%m/%Y'),
        'time': datetime.now().strftime('%X'),
        'exercise': record['name'],
        'duration': int(record['duration_min']),
        'calories': int(record['nf_calories']),
    }}
    pprint(tmp_dict)
    data_list.append(tmp_dict)

    sheety_response = requests.post(url='https://api.sheety.co/47f9c4ec8268d433ed39542adf98442e/twan/workouts',
                                    json=tmp_dict, headers=sheety_headers)
    sheety_response.raise_for_status()
    pprint(sheety_response.json())

# pprint(data_list)
# [{'calories': 440.66,
#   'date': '28/12/2021',
#   'duration': 53.63,
#   'exercise': 'bicycling',
#   'time': 53.63}]


response_sheety = requests.get(url=SHEETY_API_URL, headers=sheety_headers)
response_sheety.raise_for_status()
print('-----------------------------------------------')
pprint(response_sheety.json())
# {'workouts': [{'date': '21/07/2020', 'time': '15:00:00', 'exercise': 'Running',
# 'duration': 22, 'calories': 130, 'id': 2}]}
